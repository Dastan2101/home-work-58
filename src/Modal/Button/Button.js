import React from 'react';

const Button = (props) => {
    return (
        props.array.map((button, key) => {
            return (
                <button
                    type="button"
                    key={key}
                    className={['btn btn-', button.type].join('')}
                    onClick={button.clicked}>{button.label}</button>
            )
        })
    );
};

export default Button;