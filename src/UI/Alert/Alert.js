import React from 'react';
import './Alert.css'

const Alert = (props) => {
    return (
        props.state ? <div className={['alert alert-', props.type].join('')} role="alert">
                <h4>Some Alert text</h4>
            <button type="button" className={['btn btn-', props.type].join('')} onClick={props.closeAlert}>Close</button>
            </div> : null
    );
};

export default Alert;