import React, { Component } from 'react';
import './App.css';
import Modal from '../Modal/Modal'
import Alert from "../UI/Alert/Alert";
class App extends Component {
  state = {
    show: false,
      alert: false,
      type: '',

  };


  showModal = () => {
    this.setState({show: true})
  };

    closeModal = () => {
      this.setState({show: false})
    };

    showAlert = (type) => {
        this.setState({alert: true, type: type})
    };

    dismiss = () => {
        this.setState({alert: false})
    };

    continued = () => {
    alert('You decide to continue')
    };

    configuration = [
        {type: 'primary', label: 'Continue', clicked: this.continued},
        {type: 'danger', label: 'Close', clicked: this.closeModal},
        ];


  render() {
    return (
      <div className="App">
          <div className="modal-div">
          <button onClick={this.showModal}>Show modal</button>
        <Modal
            configuration={this.configuration}
              show={this.state.show}
              showModal={this.showModal}
              closeModal={this.closeModal}

              title="This is modal window"

          >
            <p>Here should be some text</p>
          </Modal>
          </div>
          <div className="alert-div">
              <button className="btn btn-primary"  onClick={() => this.showAlert("primary")}>Primary</button>
              <button className="btn btn-success" onClick={() => this.showAlert("success")}>Success</button>
              <button className="btn btn-danger" onClick={() => this.showAlert("danger")}>Danger</button>
              <button className="btn btn-warning" onClick={() => this.showAlert("warning")}>Warning</button>
              <Alert
                  state={this.state.alert}
                  type={this.state.type}
                  closeAlert={this.dismiss} />
          </div>
      </div>
    );
  }
}

export default App;
